package com.example.proiectmobiledev.main.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.proiectmobiledev.R;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class CurrentFragment {

    private static final List<FragmentNavigationEntry> FRAGMENT_NAVIGATION_LIST = Arrays.asList(
            new FragmentNavigationEntry(R.id.item_favorites, R.id.item_popular, R.id.favorites_to_popular),
            new FragmentNavigationEntry(R.id.item_favorites, R.id.item_all, R.id.favorites_to_all),
            new FragmentNavigationEntry(R.id.item_popular, R.id.item_favorites, R.id.popular_to_favorites),
            new FragmentNavigationEntry(R.id.item_popular, R.id.item_all, R.id.popular_to_all),
            new FragmentNavigationEntry(R.id.item_all, R.id.item_favorites, R.id.all_to_favorites),
            new FragmentNavigationEntry(R.id.item_all, R.id.item_popular, R.id.all_to_popular));

    @NotNull
    private final List<Integer> previousFragmentIds = new ArrayList<>(10);

    @NonNull
    private Integer currentFragmentId;

    @Nullable
    private Integer nextFragmentId;

    public CurrentFragment(@NonNull final Integer currentFragmentId) {
        this.currentFragmentId = currentFragmentId;
    }

    public void updateCurrentFragmentAfterForwardNavigation() {
        this.previousFragmentIds.add(currentFragmentId);
        this.currentFragmentId = this.nextFragmentId;
        this.nextFragmentId = null;
    }

    public void updateCurrentFragmentAfterBackwardNavigation() {
        this.currentFragmentId = this.previousFragmentIds.get(getLastIndex());
        this.previousFragmentIds.remove(getLastIndex());
        this.nextFragmentId = null;
    }

    public void setNextFragmentId(@NonNull final Integer nextFragmentId) {
        this.nextFragmentId = nextFragmentId;
    }

    @NonNull
    public Integer getCurrentFragmentId() {
        return currentFragmentId;
    }

    public Integer getPreviousFragmentId() {
        final int lastIndex = getLastIndex();
        if (lastIndex < 0) {
            return null;
        }

        return FRAGMENT_NAVIGATION_LIST
                .stream()
                .filter(filterFromCurrentToPrevious())
                .findFirst()
                .map(FragmentNavigationEntry::getNavigationId)
                .orElseThrow(throwNavigationIdNotFoundException());
    }

    public int getNavigationId() {
        if (nextFragmentId == null) {
            throw new RuntimeException("Can't get navigationId without nextFragmentId!");
        }

        return FRAGMENT_NAVIGATION_LIST
                .stream()
                .filter(filterFromCurrentToNext())
                .findFirst()
                .map(FragmentNavigationEntry::getNavigationId)
                .orElseThrow(throwNavigationIdNotFoundException());
    }

    private Predicate<FragmentNavigationEntry> filterFromCurrentToNext() {
        return entry -> entry.getFromId().equals(currentFragmentId)
                && entry.getToId().equals(nextFragmentId);
    }

    private Predicate<FragmentNavigationEntry> filterFromCurrentToPrevious() {
        return entry -> entry.getFromId().equals(currentFragmentId)
                && entry.getToId().equals(previousFragmentIds.get(getLastIndex()));
    }

    private Supplier<RuntimeException> throwNavigationIdNotFoundException() {
        return () -> new RuntimeException("Navigation id not found form " + currentFragmentId + " to " + nextFragmentId);
    }

    private int getLastIndex() {
        return (this.previousFragmentIds.size() - 1);
    }
}
