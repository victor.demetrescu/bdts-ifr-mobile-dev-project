package com.example.proiectmobiledev.main.ui.all;

import com.example.proiectmobiledev.main.model.Category;

public interface CategoryItemClickListener {

    void onCategoryItemClick(Category category);
}
