package com.example.proiectmobiledev.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.NavigationUI;

import com.example.proiectmobiledev.R;
import com.example.proiectmobiledev.databinding.ActivityMainBinding;
import com.example.proiectmobiledev.main.camera.CameraActivity;
import com.example.proiectmobiledev.main.model.CurrentFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding view;
    private BottomNavigationView bottomNavigationView;
    private NavController navController;
    private CurrentFragment currentFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeView();
        initializeNavigation();
        initializeToolbar();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.top_toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.item_image_capture) {
            startActivity(new Intent(this, CameraActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initializeView() {
        view = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(view.getRoot());
    }

    private void initializeNavigation() {
        bottomNavigationView = view.navView;

        final NavHostFragment navHostFragment = (NavHostFragment) getSupportFragmentManager()
                .findFragmentById(R.id.nav_host_fragment_activity_main);

        if (navHostFragment == null) {
            throw new RuntimeException("Navigation host fragment is null!");
        }

        navController = navHostFragment.getNavController();
        NavigationUI.setupWithNavController(bottomNavigationView, navController);

        currentFragment = new CurrentFragment(R.id.item_favorites);

        bottomNavigationView.setOnNavigationItemSelectedListener(item -> {
            if (currentFragment.getCurrentFragmentId() != item.getItemId()) {
                currentFragment.setNextFragmentId(item.getItemId());
                navController.navigate(currentFragment.getNavigationId());
                currentFragment.updateCurrentFragmentAfterForwardNavigation();

                if (currentFragment.getPreviousFragmentId() != null) {
                    final ActionBar supportActionBar = getSupportActionBar();
                    supportActionBar.setDisplayHomeAsUpEnabled(true);
                    supportActionBar.setDisplayShowHomeEnabled(true);
                }

                return true;
            }
            return false;
        });
    }

    public void initializeToolbar() {
        final Toolbar toolbar = findViewById(R.id.top_toolbar_test);
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onSupportNavigateUp() {
        navController.navigate(currentFragment.getPreviousFragmentId());
        currentFragment.updateCurrentFragmentAfterBackwardNavigation();
        bottomNavigationView.setSelectedItemId(currentFragment.getCurrentFragmentId());

        if (currentFragment.getPreviousFragmentId() == null) {
            final ActionBar supportActionBar = getSupportActionBar();
            supportActionBar.setDisplayHomeAsUpEnabled(false);
            supportActionBar.setDisplayShowHomeEnabled(false);
        }
        return true;
    }
}