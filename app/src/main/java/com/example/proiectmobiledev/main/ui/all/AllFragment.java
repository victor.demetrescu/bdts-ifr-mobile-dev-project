package com.example.proiectmobiledev.main.ui.all;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.proiectmobiledev.R;
import com.example.proiectmobiledev.databinding.FragmentAllBinding;
import com.example.proiectmobiledev.main.model.Category;

import java.util.Arrays;
import java.util.List;


public class AllFragment extends Fragment {

    private AllViewModel allViewModel;
    private FragmentAllBinding view;

    private EditText searchInput;
    private RecyclerView categoriesRecyclerView;

    private static final List<Category> CATEGORIES = Arrays.asList(
            new Category(R.drawable.ic_android_black_24dp, "Abstract", 5L),
            new Category(R.drawable.ic_baseline_brush_24, "Art", 20L),
            new Category(R.drawable.ic_baseline_book_24, "Books", 100L),
            new Category(R.drawable.ic_baseline_food_bank_24, "Cooking", 45L),
            new Category(R.drawable.ic_baseline_directions_car_24, "Cars", 1550L),
            new Category(R.drawable.ic_baseline_accessibility_new_24, "Disco", 930L),
            new Category(R.drawable.ic_baseline_eco_24, "Eco", 0L),
            new Category(R.drawable.ic_baseline_beach_access_24, "Fashion", 890L),
            new Category(R.drawable.ic_baseline_sports_soccer_24, "Football", 160L),
            new Category(R.drawable.ic_android_black_24dp, "Gourmet", 10L),
            new Category(R.drawable.ic_android_black_24dp, "History", 200L),
            new Category(R.drawable.ic_android_black_24dp, "Indie", 33L),
            new Category(R.drawable.ic_android_black_24dp, "Joyful", 5400L),
            new Category(R.drawable.ic_android_black_24dp, "Sports", 50L),
            new Category(R.drawable.ic_android_black_24dp, "Science", 700000L),
            new Category(R.drawable.ic_android_black_24dp, "Nature", 11L),
            new Category(R.drawable.ic_android_black_24dp, "Tennis", 990L),
            new Category(R.drawable.ic_android_black_24dp, "Programming", 4000L),
            new Category(R.drawable.ic_android_black_24dp, "Racing", 100L),
            new Category(R.drawable.ic_android_black_24dp, "Politics", 8L)
    );

    private CategoryAdapter categoryAdapter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {

        categoryAdapter = new CategoryAdapter(CATEGORIES, category -> {
            final CategoryDialog categoryDialog = new CategoryDialog(getContext());
            categoryDialog.setTitle(category.getName());
            categoryDialog.setMessage("Count: " + category.getCount());
            categoryDialog.setButton(DialogInterface.BUTTON_NEUTRAL, "Share", (dialog, which) -> openShareIntent(category));
            categoryDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Close", (dialog, which) -> categoryDialog.hide());
            categoryDialog.show();
        });
        allViewModel = new ViewModelProvider(this).get(AllViewModel.class);

        view = FragmentAllBinding.inflate(inflater, container, false);
        View root = view.getRoot();

        initializeSearchInput();
        initializeRecyclerView();

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        view = null;
    }

    private void initializeSearchInput() {
        searchInput = view.searchCategoriesText;
        final TextWatcher textWatcher = createTextWatcher();
        searchInput.addTextChangedListener(textWatcher);
    }

    private TextWatcher createTextWatcher() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                searchInputChanged();
            }
        };
    }

    private void searchInputChanged() {
        final String searchText = searchInput.getText().toString();
        categoryAdapter.filterCategories(searchText);
    }

    private void initializeRecyclerView() {
        categoriesRecyclerView = view.allCategories;
        categoriesRecyclerView.setAdapter(categoryAdapter);
        categoriesRecyclerView.setLayoutManager(new LinearLayoutManager(view.getRoot().getContext()));

        categoriesRecyclerView.addItemDecoration(new DividerItemDecoration(categoriesRecyclerView.getContext(), DividerItemDecoration.VERTICAL));
    }

    private void openShareIntent(final Category category) {
        final Uri imageURI = Uri.parse(getSharedFilePath());
        final Intent shareIntent = buildShareIntent(imageURI, category);
        startActivity(Intent.createChooser(shareIntent, "Share " + category.getName()));
    }

    private String getSharedFilePath() {
        final String searchedFileName = "android.png";
        final String[] searchProjection = {MediaStore.Images.Media.DATA};
        final Cursor cursor = getActivity().getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                searchProjection,
                null,
                null,
                null);

        final int cursorColumnIndex = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
        String fileName = null;

        while (cursor.moveToNext()) {
            fileName = cursor.getString(cursorColumnIndex);
            if (fileName.endsWith(searchedFileName)) {
                break;
            }
        }
        return fileName;
    }

    private Intent buildShareIntent(final Uri imageURI,
                                    final Category category) {
        final Intent shareIntent = new Intent();

        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT, "Checkout this awesome category: " + category.getName());
        shareIntent.putExtra(Intent.EXTRA_STREAM, imageURI);
        shareIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        shareIntent.setFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        shareIntent.setType("image/png");

        return shareIntent;
    }

}