package com.example.proiectmobiledev.main.ui.all;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.proiectmobiledev.R;
import com.example.proiectmobiledev.main.model.Category;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryViewHolder> {

    private final List<Category> allCategories;
    private final List<Category> categories;
    private final CategoryItemClickListener clickListener;
    private CategoryViewHolder categoryViewHolder;

    public CategoryAdapter(final List<Category> categories,
                           final CategoryItemClickListener clickListener) {
        this.allCategories = categories;
        this.categories = new ArrayList<>(categories);
        this.clickListener = clickListener;
    }

    @NotNull
    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup parent,
                                                 int viewType) {
        final Context context = parent.getContext();
        final LayoutInflater inflater = LayoutInflater.from(context);
        final View contactView = inflater.inflate(R.layout.category_item, parent, false);

        return new CategoryViewHolder(contactView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(CategoryViewHolder holder,
                                 int position) {
        this.categoryViewHolder = holder;
        final Category currentCategory = categories.get(position);

        final ImageView categoryIcon = holder.categoryIcon;
        final TextView categoryName = categoryViewHolder.categoryName;
        final TextView categoryCount = categoryViewHolder.categoryCount;

        categoryIcon.setImageResource(currentCategory.getImageResourceId());
        categoryName.setText(currentCategory.getName());
        categoryCount.setText("(" + currentCategory.getCount() + ")");

        categoryViewHolder.registerItemListener(categories.get(position), clickListener);
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public void filterCategories(final String searchInput) {
        if (isNull(searchInput) || searchInput.isEmpty() || " ".equals(searchInput)) {
            updateDisplayedCategories(allCategories);
            return;
        }

        final List<Category> filteredCategories = this.allCategories
                .stream()
                .filter(category -> category.getName().toLowerCase().contains(searchInput.toLowerCase()))
                .sorted(Comparator.comparing(Category::getName))
                .collect(Collectors.toList());

       updateDisplayedCategories(filteredCategories);
    }

    private void updateDisplayedCategories(final List<Category> categoriesToBeDisplayed) {
        this.categories.clear();
        this.categories.addAll(categoriesToBeDisplayed);
        notifyDataSetChanged();
    }

}
