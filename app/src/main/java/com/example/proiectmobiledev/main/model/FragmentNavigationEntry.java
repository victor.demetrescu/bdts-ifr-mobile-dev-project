package com.example.proiectmobiledev.main.model;

import androidx.annotation.NonNull;

class FragmentNavigationEntry {

    @NonNull
    private final Integer fromId;

    @NonNull
    private final Integer toId;

    @NonNull
    private final Integer navigationId;

    FragmentNavigationEntry(@NonNull final Integer fromId,
                            @NonNull final Integer toId,
                            @NonNull final Integer navigationId) {
        this.fromId = fromId;
        this.toId = toId;
        this.navigationId = navigationId;
    }

    @NonNull
    public Integer getFromId() {
        return fromId;
    }

    @NonNull
    public Integer getToId() {
        return toId;
    }

    @NonNull
    public Integer getNavigationId() {
        return navigationId;
    }
}
