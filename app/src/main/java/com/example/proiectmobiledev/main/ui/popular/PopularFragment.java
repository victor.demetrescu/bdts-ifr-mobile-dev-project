package com.example.proiectmobiledev.main.ui.popular;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.proiectmobiledev.databinding.FragmentPopularBinding;

public class PopularFragment extends Fragment {

    private PopularViewModel popularViewModel;
    private FragmentPopularBinding view;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        popularViewModel = new ViewModelProvider(this).get(PopularViewModel.class);

        view = FragmentPopularBinding.inflate(inflater, container, false);
        View root = view.getRoot();

        final TextView textView = view.textPopular;
        popularViewModel.getText().observe(getViewLifecycleOwner(), textView::setText);

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        view = null;
    }
}