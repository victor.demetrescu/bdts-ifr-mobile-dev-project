package com.example.proiectmobiledev.main.camera;

import android.hardware.Camera;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import static com.example.proiectmobiledev.main.camera.CameraUtil.MEDIA_TYPE_IMAGE;
import static com.example.proiectmobiledev.main.camera.CameraUtil.getOutputMediaFile;

public class CameraPictureCallback implements Camera.PictureCallback {

    private static final String TAG = "ImageCapture";

    private static final CameraPictureCallback INSTANCE = new CameraPictureCallback();

    public static CameraPictureCallback getInstance() {
        return INSTANCE;
    }

    private CameraPictureCallback() {

    }

    @Override
    public void onPictureTaken(final byte[] data,
                               final Camera camera) {
        final File pictureFile = getOutputMediaFile(MEDIA_TYPE_IMAGE);
        if (pictureFile == null) {
            Log.d("ImageCapture", "Error creating media file, check storage permissions");
            return;
        }

        persistImageToDevice(data, pictureFile);
    }

    private void persistImageToDevice(final byte[] data,
                                      final File pictureFile) {
        try {
            final FileOutputStream fileOutputStream = new FileOutputStream(pictureFile);
            fileOutputStream.write(data);
            fileOutputStream.close();
        } catch (FileNotFoundException e) {
            Log.d(TAG, "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d(TAG, "Error accessing file: " + e.getMessage());
        }
    }
}
