package com.example.proiectmobiledev.main.camera;

import android.hardware.Camera;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.proiectmobiledev.databinding.ActivityCameraBinding;

public class CameraActivity extends AppCompatActivity {

    private ActivityCameraBinding view;

    private Camera camera;
    private CameraPreview cameraPreview;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeView();
        registerCamera();
        registerButtonListeners();
    }

    @Override
    public void onPause() {
        super.onPause();
        releaseCamera();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        releaseCamera();
    }

    private void initializeView() {
        view = ActivityCameraBinding.inflate(getLayoutInflater());
        setContentView(view.getRoot());
    }

    private void registerCamera() {
        final FrameLayout preview = view.cameraPreview;

        camera = getCameraInstance();
        cameraPreview = new CameraPreview(getBaseContext(), camera);
        preview.addView(cameraPreview);


    }

    private Camera getCamera() {
        if (camera == null) {
            camera = getCameraInstance();
        }
        return camera;
    }

    private Camera getCameraInstance() {
        Camera c = null;
        try {
            c = Camera.open();
        } catch (Exception e) {
            e.getStackTrace();
        }
        return c;
    }

    private void releaseCamera() {
        if (camera != null) {
            camera.release();
            camera = null;
        }
    }

    private void registerButtonListeners() {
        final Button captureButton = view.buttonCapture;
        captureButton.setOnClickListener(v -> {
            getCamera().takePicture(null, null, CameraPictureCallback.getInstance());
        });

        final Button backButton = view.buttonBack;
        backButton.setOnClickListener(v -> {
            onBackPressed();
            Toast toast = Toast.makeText(CameraActivity.this, "getBaseContext()", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER_HORIZONTAL,0,0);
            toast.show();
        });
    }

}
