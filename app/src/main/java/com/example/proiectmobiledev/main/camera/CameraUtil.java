package com.example.proiectmobiledev.main.camera;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import static java.text.DateFormat.getDateInstance;
import static java.text.DateFormat.getDateTimeInstance;

public class CameraUtil {

    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;

    public static final String IMAGES_FOLDER = "ProiectMobileDEV";
    private static final String IMAGE_PREFIX = "IMG_";
    private static final String IMAGE_SUFFIX = ".jpg";
    private static final String VIDEO_PREFIX = "VID_";
    private static final String VIDEO_SUFFIX = ".mp4";
    private static final String TIMESTAMP_FORMAT = "yyyyMMddHHmmss";

    public static File getOutputMediaFile(int mediaFileType) {
        final File mediaStorageDirectory = new File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGES_FOLDER);

        if (!checkMediaStorageDirectoryExists(mediaStorageDirectory)) {
            return null;
        }

        final String mediaFilePath = getMediaFilePath(mediaStorageDirectory, mediaFileType);
        return (mediaFilePath != null) ? new File(mediaFilePath) : null;
    }

    private static boolean checkMediaStorageDirectoryExists(final File mediaStorageDirectory) {
        if (!mediaStorageDirectory.exists()) {
            if (!mediaStorageDirectory.mkdirs()) {
                Log.d(IMAGES_FOLDER, "failed to create directory");
                return false;
            }
        }
        return true;
    }

    private static String getMediaFilePath(final File mediaStorageDirectory,
                                           final int mediaFileType) {
        if (mediaFileType == MEDIA_TYPE_IMAGE) {
            return buildMediaFilePath(mediaStorageDirectory, IMAGE_PREFIX, IMAGE_SUFFIX);
        } else if (mediaFileType == MEDIA_TYPE_VIDEO) {
            return buildMediaFilePath(mediaStorageDirectory, VIDEO_PREFIX, VIDEO_SUFFIX);
        }

        return null;
    }

    private static String buildMediaFilePath(final File mediaStorageDirectory,
                                             final String mediaTypePrefix,
                                             final String mediaTypeSuffix) {
        return new StringBuilder(mediaStorageDirectory.getPath())
                .append(File.separator)
                .append(mediaTypePrefix)
                .append(getFormattedTimestamp())
                .append(mediaTypeSuffix)
                .toString()
                .replaceAll(" ", "_")
                .trim();
    }

    private static String getFormattedTimestamp() {
        return new SimpleDateFormat(TIMESTAMP_FORMAT).format(new Date());
    }

}
