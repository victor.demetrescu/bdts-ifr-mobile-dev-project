package com.example.proiectmobiledev.main.model;

import androidx.annotation.NonNull;

import org.jetbrains.annotations.NotNull;

public class Category {

    @NonNull
    private Integer imageResourceId;

    @NonNull
    private final String name;

    @NonNull
    private final Long count;

    public Category(@NonNull final Integer imageResourceId,
                    @NonNull final String name,
                    @NonNull final Long count) {
        this.imageResourceId = imageResourceId;
        this.name = name;
        this.count = count;
    }

    @NotNull
    public Integer getImageResourceId() {
        return imageResourceId;
    }

    @NonNull
    public String getName() {
        return name;
    }

    @NonNull
    public Long getCount() {
        return count;
    }
}
