package com.example.proiectmobiledev.main.ui.all;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.proiectmobiledev.R;
import com.example.proiectmobiledev.main.model.Category;

public class CategoryViewHolder extends RecyclerView.ViewHolder {

    public ImageView categoryIcon;
    public TextView categoryName;
    public TextView categoryCount;

    public CategoryViewHolder(final View itemView) {
        super(itemView);
        this.categoryIcon = itemView.findViewById(R.id.category_icon);
        this.categoryName = itemView.findViewById(R.id.category_name);
        this.categoryCount = itemView.findViewById(R.id.category_count);
    }

    public void registerItemListener(final Category category, final CategoryItemClickListener listener) {
        itemView.setOnClickListener(v -> listener.onCategoryItemClick(category));
    }
}
