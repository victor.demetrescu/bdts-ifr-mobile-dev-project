package com.example.proiectmobiledev.model;

import androidx.annotation.Nullable;

public class GenericActionResult {

    @Nullable
    protected Integer error;

    protected boolean isSuccessful;

    protected GenericActionResult() {

    }

    protected GenericActionResult(@Nullable final Integer error) {
        this.error = error;
        this.isSuccessful = false;
    }

    @Nullable
    public Integer getError() {
        return error;
    }

    public boolean isSuccessful() {
        return isSuccessful;
    }
}
