package com.example.proiectmobiledev.service;

import android.content.Context;

import androidx.room.Room;

import com.example.proiectmobiledev.db.ApplicationDatabase;
import com.example.proiectmobiledev.login.service.AuthenticationService;
import com.example.proiectmobiledev.login.service.impl.AuthenticationServiceImpl;

public class InstanceHolder {

    private static final String DATABASE_NAME = "mobile_dev";
    private static final InstanceHolder INSTANCE = new InstanceHolder();

    public static InstanceHolder getInstance() {
        return INSTANCE;
    }

    private final AuthenticationService authenticationService = AuthenticationServiceImpl.getInstance();
    private ApplicationDatabase applicationDatabase;

    public void initializeDatabase(final Context context) {
        this.applicationDatabase = Room
                .databaseBuilder(context, ApplicationDatabase.class, DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .build();
    }

    public AuthenticationService getAuthenticationService() {
        return this.authenticationService;
    }

    public ApplicationDatabase getDatabase() {
        return this.applicationDatabase;
    }
}
