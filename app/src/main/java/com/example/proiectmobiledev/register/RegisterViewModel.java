package com.example.proiectmobiledev.register;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.proiectmobiledev.R;
import com.example.proiectmobiledev.db.model.User;
import com.example.proiectmobiledev.login.model.Credentials;
import com.example.proiectmobiledev.login.service.AuthenticationService;
import com.example.proiectmobiledev.login.ui.model.LoginResult;
import com.example.proiectmobiledev.service.InstanceHolder;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.functions.Action;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class RegisterViewModel extends ViewModel {

    private final MutableLiveData<RegisterResult> registerResult = new MutableLiveData<>();
    private final AuthenticationService authenticationService = InstanceHolder.getInstance().getAuthenticationService();

    public RegisterViewModel() {
    }

    public LiveData<RegisterResult> getRegisterResult() {
        return registerResult;
    }

    public void register(final String email,
                         final String username,
                         final String password) {
        final User user = new User();

        user.setEmail(email);
        user.setUsername(username);
        user.setPassword(Credentials.withUsernameAndPassword(username, password).getHashedPassword());

        authenticationService
                .register(user)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(registrationSuccessful(), registrationFailed());
    }

    private Action registrationSuccessful() {
        return () -> {
            System.out.println("DONE!");
            registerResult.setValue(RegisterResult.withSuccess());
        };
    }

    private Consumer<Throwable> registrationFailed() {
        return (error) -> {
            error.printStackTrace();
            registerResult.setValue(RegisterResult.withFailure());
        };
    }
}
