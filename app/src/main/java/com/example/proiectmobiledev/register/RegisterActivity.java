package com.example.proiectmobiledev.register;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;

import com.example.proiectmobiledev.R;
import com.example.proiectmobiledev.databinding.ActivityRegisterBinding;
import com.example.proiectmobiledev.login.LoginActivity;
import com.example.proiectmobiledev.login.ui.LoggedInUserView;
import com.example.proiectmobiledev.main.MainActivity;


public class RegisterActivity extends AppCompatActivity {

    private ActivityRegisterBinding view;
    private RegisterViewModel registerViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeView();
        registerListeners();
    }

    private void initializeView() {
        view = ActivityRegisterBinding.inflate(getLayoutInflater());
        setContentView(view.getRoot());

        registerViewModel = new RegisterViewModel();
    }

    private void registerListeners() {
        final EditText email = view.registerEmail;
        final EditText username = view.registerUsername;
        final EditText password = view.registerPassword;
        final EditText passwordRepeat = view.registerPasswordRepeat;
        final Button registerButton = view.registerButton;

        registerViewModel.getRegisterResult().observe(this, registerResult -> {
            if (registerResult == null) {
                return;
            }
            if (!registerResult.isSuccessful()) {
                showRegisterFailed(registerResult.getError());
            } else {
                goToLoginPage();
                setResult(Activity.RESULT_OK);
                finish();
            }
        });

        passwordRepeat.setOnEditorActionListener((onEditorActionListener, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                view.failedRegisterText.setVisibility(View.INVISIBLE);
                registerViewModel.register(
                        email.getText().toString(),
                        username.getText().toString(),
                        password.getText().toString());
            }
            return false;
        });

        registerButton.setOnClickListener(onClickListener -> {
            view.failedRegisterText.setVisibility(View.INVISIBLE);
            registerViewModel.register(
                    email.getText().toString(),
                    username.getText().toString(),
                    password.getText().toString());
        });
    }

    private void showRegisterFailed(@StringRes Integer errorString) {
        view.failedRegisterText.setVisibility(View.VISIBLE);
        Toast.makeText(getApplicationContext(), errorString, Toast.LENGTH_SHORT).show();
    }

    private void goToLoginPage() {
        startActivity(new Intent(this, LoginActivity.class));
    }

}
