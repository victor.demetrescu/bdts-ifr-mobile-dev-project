package com.example.proiectmobiledev.register;

import android.content.Intent;

import androidx.annotation.Nullable;

import com.example.proiectmobiledev.R;
import com.example.proiectmobiledev.model.GenericActionResult;

public class RegisterResult extends GenericActionResult {

    public static RegisterResult withSuccess() {
        return new RegisterResult();
    }

    public static RegisterResult withFailure() {
        return new RegisterResult(R.string.register_failed);
    }

    private RegisterResult(@Nullable final Integer error) {
        super(error);
    }

    private RegisterResult() {
        this.isSuccessful = true;
    }
}
