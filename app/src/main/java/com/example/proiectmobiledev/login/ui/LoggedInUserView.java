package com.example.proiectmobiledev.login.ui;

/**
 * Class exposing authenticated user details to the UI.
 */
public class LoggedInUserView {

    private final String displayName;

    public LoggedInUserView(final String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}