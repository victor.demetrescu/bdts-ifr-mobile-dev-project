package com.example.proiectmobiledev.login.model;

import androidx.annotation.NonNull;

import org.jetbrains.annotations.NotNull;

public class AuthenticatedUser {

    @NonNull
    private final String username;

    @NonNull
    private final String name;

    public AuthenticatedUser(@NotNull final String displayName,
                             @NonNull final String name) {
        this.username = displayName;
        this.name = name;
    }

    @NotNull
    public String getUsername() {
        return username;
    }

    @NonNull
    public String getName() {
        return name;
    }
}