package com.example.proiectmobiledev.login.ui;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.proiectmobiledev.R;
import com.example.proiectmobiledev.db.model.User;
import com.example.proiectmobiledev.login.model.Credentials;
import com.example.proiectmobiledev.login.service.AuthenticationService;
import com.example.proiectmobiledev.login.ui.model.LoginFormState;
import com.example.proiectmobiledev.login.ui.model.LoginResult;
import com.example.proiectmobiledev.service.InstanceHolder;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class LoginViewModel extends ViewModel {

    private final MutableLiveData<LoginFormState> loginFormState = new MutableLiveData<>();
    private final MutableLiveData<LoginResult> loginResult = new MutableLiveData<>();
    private final AuthenticationService authenticationService = InstanceHolder.getInstance().getAuthenticationService();

    public LoginViewModel() {
    }

    public LiveData<LoginFormState> getLoginFormState() {
        return loginFormState;
    }

    public LiveData<LoginResult> getLoginResult() {
        return loginResult;
    }

    public void login(final Credentials credentials) {
        authenticationService
                .login(credentials)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(userFound(), userNotFound());
    }

    public void loginDataChanged(final Credentials credentials) {
        if (!isUserNameValid(credentials.getUsername())) {
            loginFormState.setValue(new LoginFormState(R.string.invalid_username, null));
        } else if (!isPasswordValid(credentials.getPassword())) {
            loginFormState.setValue(new LoginFormState(null, R.string.invalid_password));
        } else {
            loginFormState.setValue(new LoginFormState(true));
        }
    }

    private boolean isUserNameValid(final String username) {
        return ((username != null) && !username.isEmpty());
//        if (username.contains("@")) {
//            return Patterns.EMAIL_ADDRESS.matcher(username).matches();
//        }
    }

    private boolean isPasswordValid(final String password) {
        return ((password != null) && (password.trim().length() > 5));
    }

    private Consumer<User> userFound() {
        return foundUser -> loginResult.setValue(LoginResult.withSuccess(foundUser.getUsername()));
    }

    private Consumer<Throwable> userNotFound() {
        return error -> loginResult.setValue(LoginResult.withError(R.string.login_failed));
    }
}