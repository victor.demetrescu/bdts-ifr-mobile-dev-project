package com.example.proiectmobiledev.login.service.impl;

import com.example.proiectmobiledev.db.dao.UserDAO;
import com.example.proiectmobiledev.db.model.User;
import com.example.proiectmobiledev.login.model.AuthenticationResult;
import com.example.proiectmobiledev.login.model.Credentials;
import com.example.proiectmobiledev.login.service.AuthenticationService;
import com.example.proiectmobiledev.service.InstanceHolder;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class AuthenticationServiceImpl implements AuthenticationService {

    private static final AuthenticationServiceImpl INSTANCE = new AuthenticationServiceImpl();

    public static AuthenticationServiceImpl getInstance() {
        return INSTANCE;
    }

    private AuthenticationServiceImpl() {

    }

    @Override
    public Single<User> login(final Credentials credentials) {
        return getUserDAO().findByUsernameAndPassword(credentials.getUsername(), credentials.getHashedPassword());
    }

    @Override
    public Completable register(final User user) {
        return getUserDAO().insertUser(user);
    }

    private Single<User> checkIfUserExists(final String email, final String username) {
        return getUserDAO().findByEmailOrUsername(email, username);
    }

    @Override
    public AuthenticationResult logout() {
        return AuthenticationResult.successful();
    }

    private UserDAO getUserDAO() {
        return InstanceHolder
                .getInstance()
                .getDatabase()
                .userDao();
    }

}
