package com.example.proiectmobiledev.login;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;

import com.example.proiectmobiledev.R;
import com.example.proiectmobiledev.databinding.ActivityLoginBinding;
import com.example.proiectmobiledev.login.model.Credentials;
import com.example.proiectmobiledev.login.ui.LoggedInUserView;
import com.example.proiectmobiledev.login.ui.LoginViewModel;
import com.example.proiectmobiledev.main.MainActivity;
import com.example.proiectmobiledev.register.RegisterActivity;
import com.example.proiectmobiledev.service.InstanceHolder;

public class LoginActivity extends AppCompatActivity {

    private ActivityLoginBinding view;
    private LoginViewModel loginViewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeView();
        registerListeners();
        initializeDatabase();
    }

    private void initializeView() {
        view = ActivityLoginBinding.inflate(getLayoutInflater());
        setContentView(view.getRoot());

        loginViewModel = new LoginViewModel();
    }

    private void registerListeners() {
        final EditText usernameEditText = view.username;
        final EditText passwordEditText = view.password;
        final Button loginButton = view.loginButton;
        final Button registerButton = view.registerButton;
        final ProgressBar loadingProgressBar = view.loading;

        loginViewModel.getLoginFormState().observe(this, loginFormState -> {
            if (loginFormState == null) {
                return;
            }
            loginButton.setEnabled(loginFormState.isDataValid());
            if (loginFormState.getUsernameError() != null) {
                usernameEditText.setError(getString(loginFormState.getUsernameError()));
            }
            if (loginFormState.getPasswordError() != null) {
                passwordEditText.setError(getString(loginFormState.getPasswordError()));
            }
        });

        loginViewModel.getLoginResult().observe(this, loginResult -> {
            if (loginResult == null) {
                return;
            }
            loadingProgressBar.setVisibility(View.GONE);
            if (!loginResult.isSuccessful()) {
                showLoginFailed(loginResult.getError());
            } else {
                assert loginResult.getUserData() != null;
                openMainActivity(loginResult.getUserData());
                setResult(Activity.RESULT_OK);
                finish();
            }
        });

        TextWatcher afterTextChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence c, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence c, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                loginViewModel.loginDataChanged(Credentials.withUsernameAndPassword(
                        usernameEditText.getText().toString(),
                        passwordEditText.getText().toString()));
            }
        };

        usernameEditText.addTextChangedListener(afterTextChangedListener);
        passwordEditText.addTextChangedListener(afterTextChangedListener);
        passwordEditText.setOnEditorActionListener((onEditorActionListener, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                view.failedLoginText.setVisibility(View.INVISIBLE);
                loginViewModel.login(Credentials.withUsernameAndPassword(
                        usernameEditText.getText().toString(),
                        passwordEditText.getText().toString()));
            }
            return false;
        });

        loginButton.setOnClickListener(onClickListener -> {
            view.failedLoginText.setVisibility(View.INVISIBLE);
            loadingProgressBar.setVisibility(View.VISIBLE);
            loginViewModel.login(Credentials.withUsernameAndPassword(
                    usernameEditText.getText().toString(),
                    passwordEditText.getText().toString()));
        });

        registerButton.setOnClickListener(onClickListener -> {
            startActivity(new Intent(this, RegisterActivity.class));
        });
    }

    private void initializeDatabase() {
        InstanceHolder
                .getInstance()
                .initializeDatabase(getApplicationContext());
    }

    private void showLoginFailed(@StringRes Integer errorString) {
        view.failedLoginText.setVisibility(View.VISIBLE);
        Toast.makeText(getApplicationContext(), errorString, Toast.LENGTH_SHORT).show();
    }

    private void openMainActivity(final LoggedInUserView model) {
        final String welcome = getString(R.string.welcome) + model.getDisplayName();
        Toast.makeText(getApplicationContext(), welcome, Toast.LENGTH_LONG).show();
        startActivity(new Intent(this, MainActivity.class));
    }
}