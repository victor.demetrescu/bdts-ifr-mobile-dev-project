package com.example.proiectmobiledev.login.service;

import com.example.proiectmobiledev.db.model.User;
import com.example.proiectmobiledev.login.model.AuthenticationResult;
import com.example.proiectmobiledev.login.model.Credentials;

import io.reactivex.Observable;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.Single;

public interface AuthenticationService {

    Single<User> login(final Credentials credentials);

    Completable register(final User user);

    AuthenticationResult logout();
}
