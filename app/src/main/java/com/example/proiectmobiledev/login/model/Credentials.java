package com.example.proiectmobiledev.login.model;

import androidx.annotation.NonNull;

import org.jetbrains.annotations.NotNull;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class Credentials {

    @NonNull
    private final String username;

    @NonNull
    private final String password;

    @NonNull
    private final String hashedPassword;

    public static Credentials withUsernameAndPassword(final String username,
                                                      final String password) {
        return new Credentials(username, password);
    }

    private Credentials(@NotNull final String username,
                        @NotNull final String password) {
        this.username = username;
        this.password = password;
        this.hashedPassword = hashPassword();
    }

    @NotNull
    public String getUsername() {
        return this.username;
    }

    @NotNull
    public String getPassword() {
        return this.password;
    }

    @NotNull
    public String getHashedPassword() {
        return this.hashedPassword;
    }

    private String hashPassword() {
        try {
            final MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(this.password.getBytes());
            final byte[] bytes = md.digest();
            StringBuilder sb = new StringBuilder();
            for (byte aByte : bytes) {
                sb.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to hash password");
        }
    }
}
