package com.example.proiectmobiledev.login.model;

public class AuthenticationResult {

    private final boolean isSuccessful;
    private final AuthenticatedUser user;

    private AuthenticationResult(final boolean isSuccessful,
                                 final AuthenticatedUser user) {
        this.isSuccessful = isSuccessful;
        this.user = user;
    }

    public static AuthenticationResult successful() {
        return new AuthenticationResult(true, null);
    }

    public static AuthenticationResult successful(final AuthenticatedUser user) {
        return new AuthenticationResult(true, user);
    }

    public static AuthenticationResult failed() {
        return new AuthenticationResult(false, null);
    }

    public boolean isSuccessful() {
        return isSuccessful;
    }

    public AuthenticatedUser getUser() {
        return user;
    }
}
