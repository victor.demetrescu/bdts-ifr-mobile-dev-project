package com.example.proiectmobiledev.login.ui.model;

import androidx.annotation.Nullable;

import com.example.proiectmobiledev.login.ui.LoggedInUserView;
import com.example.proiectmobiledev.model.GenericActionResult;

public class LoginResult extends GenericActionResult {

    @Nullable
    private LoggedInUserView userData;

    public static LoginResult withSuccess(final String displayName) {
        return new LoginResult(new LoggedInUserView(displayName));
    }

    public static LoginResult withError(final Integer error) {
        return new LoginResult(error);
    }

    private LoginResult(@Nullable final Integer error) {
        super(error);
    }

    private LoginResult(@Nullable final LoggedInUserView userData) {
        this.userData = userData;
        this.isSuccessful = true;
    }

    @Nullable
    public LoggedInUserView getUserData() {
        return userData;
    }
}