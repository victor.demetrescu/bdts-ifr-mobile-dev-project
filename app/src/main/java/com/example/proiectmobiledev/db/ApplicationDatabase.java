package com.example.proiectmobiledev.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.proiectmobiledev.db.dao.UserDAO;
import com.example.proiectmobiledev.db.model.User;

@Database(entities = {User.class}, version = 2)
public abstract class ApplicationDatabase extends RoomDatabase {

    public abstract UserDAO userDao();
}

