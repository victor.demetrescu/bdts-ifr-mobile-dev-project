package com.example.proiectmobiledev.db.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.proiectmobiledev.db.model.User;

import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.Single;

@Dao
public interface UserDAO {

    @Query("SELECT u.* FROM USERS u WHERE u.USERNAME = :username AND u.PASSWORD = :password")
    Single<User> findByUsernameAndPassword(final String username,
                                           final String password);

    @Query("SELECT u.* FROM USERS u WHERE u.EMAIL = :email or u.USERNAME = :username")
    Single<User> findByEmailOrUsername(final String email,
                                       final String username);

    @Insert(entity = User.class)
    Completable insertUser(final User user);

}

