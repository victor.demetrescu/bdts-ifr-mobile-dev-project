# General Description
This mobile application is written entirely in Java. Gradle is used for dependency management.

***

## 1. Launching the app
Open the project using an IDE, and run the project as an ``Android App``.

## 2. App features
The project consists of the following implementations:

* Registering and authenticating using ``Room`` as a datasource.
* Navigation through a set of menus, using ``Bottom navigation``.
* Take a picture with the device's camera via the ``android.hardware.Camera`` API.
* View categories in a ``Recycler view`` and filter them by name.
* Share content using``Andoid Sharesheet``.

### 2.1 Registering and logging in
The user must register to access the app.
The registration is done via the register page, where the user has to provide some personal data.
After a successful registration, he will be inserted in the Room DB and now he has login access, using the previously provided credentials.

![register](https://gitlab.com/victor.demetrescu/bdts-ifr-mobile-dev-project/-/blob/master/app/screens/register.png?raw=true)

![login_1](https://gitlab.com/victor.demetrescu/bdts-ifr-mobile-dev-project/-/blob/master/app/screens/login_1.png?raw=true)

![login_2](https://gitlab.com/victor.demetrescu/bdts-ifr-mobile-dev-project/-/blob/master/app/screens/login_2.png?raw=true)

### 2.2 Main menu
The main menu consists of 3 tabs, between which the user can cycle using the bottom navigation, and the top left back arrow.

![main_menu_1](https://gitlab.com/victor.demetrescu/bdts-ifr-mobile-dev-project/-/blob/master/app/screens/main_menu_1.png?raw=true)


![main_menu_3](https://gitlab.com/victor.demetrescu/bdts-ifr-mobile-dev-project/-/blob/master/app/screens/main_menu_3.png?raw=true)

In the upper section, if the user clicks on the camera, it will simulate a picture interaction, using the device's built-in camera.

![camera](https://gitlab.com/victor.demetrescu/bdts-ifr-mobile-dev-project/-/blob/master/app/screens/camera.png?raw=true)

The page called ``All`` contains a ``Recycler view`` with a search function. 

![filter](https://gitlab.com/victor.demetrescu/bdts-ifr-mobile-dev-project/-/blob/master/app/screens/filter.png?raw=true)

If the user presses on any category, it will open a menu for sharing.

![share_1](https://gitlab.com/victor.demetrescu/bdts-ifr-mobile-dev-project/-/blob/master/app/screens/share_1.png?raw=true)

![share_2](https://gitlab.com/victor.demetrescu/bdts-ifr-mobile-dev-project/-/blob/master/app/screens/share_2.png?raw=true)

